# Online_visibility_collector

![](https://gitlab.com/data-major/online_visibility_collector/badges/master/pipeline.svg) 
![](https://gitlab.com/data-major/online_visibility_collector/badges/master/coverage.svg)

## Description

*Online visibility collector* (**OVC**) is a python based standalone and schedulled ETL designed to aggregate data from various monitoring web services into one single **Time Series Database**. Currently, **OVC** only supports [InfluxDB](https://www.influxdata.com/)

Current API connections: 

* Google analytics V4 
* Google Search Console V3

For Google analytics V4 and Google Search V3, you'll need to generate **service account key file** [documentation](https://developers.google.com/analytics/devguides/reporting/core/v4/authorization) and grant it rights to those applications

## Usage

The docker image available at [registry.gitlab.com/data-major/online_visibility_collector](registry.gitlab.com/data-major/online_visibility_collector) requires the following environment variables

|variable name              |     description                                                          | example          |
|--------------------------:|:-------------------------------------------------------------------------|:----------------:|
| `DB_HOST`                 | Hostname of the Influx database                                          | somewhere.com    |
| `DB_PORT `                | Port of the Influx database                                              | 8086             |
| `INFLUXDB_DB`             | Name of the database                                                     | some_name        |
| `INFLUXDB_USER`           | Username on InfluxDB                                                     | some_user        |
| `INFLUXDB_USER_PASSWORD`  | Password from the User                                                   | some_password    |
| `GA_VIEW_ID`  %           | The ID of the **Google Analytics** view you want to ingest the data from | 608124608        |
| `GS_URL`      %           | The url of a domain you own on **Google Search Console**                 | https://some.url |

`GA_VIEW_ID` and `GS_URL` are optional, depending on whether you want to ingest data from these services.

However, for both services, you'll need to mount your google **json api key file** on `/g_service_account.json`

### Deployment with docker-compose

Here is an example of a docker-compose based environment

```yml
version: '3.7'
services:
  db:
    image: influxdb:1.7-alpine
    restart: always
    container_name: db
    networks:
      - monitoring
    volumes:
      - influxdb-volume:/var/lib/influxdb
    expose:
      - 8086
    environment: 
      - INFLUXDB_DB=some_name
      - INFLUXDB_USER=some_user
      - INFLUXDB_ADMIN_USER=some_other_user
      - INFLUXDB_ADMIN_PASSWORD=some_password
      - INFLUXDB_USER_PASSWORD=db_user_password
      
  collector:
    container_name: collector
	image: registry.gitlab.com/data-major/online_visibility_collector
    volumes:
      - ./path/to/the/google_analytics_key.json:/g_service_account.json
    networks:
      - monitoring
    ports:
      - 1312:1312
    depends_on:
      - db
    environment: 
      - DB_HOST=db
      - DB_PORT=8086
      - INFLUXDB_DB=some_name
      - INFLUXDB_USER=some_user
      - INFLUXDB_USER_PASSWORD=db_user_password
      - GA_VIEW_ID=608124608
      - GS_URL=https://some.verified.domain

networks:
  monitoring:

volumes:
  influxdb-volume:

```
