import sys
sys.path.insert(1, '/usr/src/collector')
import pytest, unittest, os, logging
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
from unittest.mock import patch, Mock, MagicMock
from collector import *
from utils import *

ga_api_mock     = MagicMock(return_value=json.loads(open('../test/api_responses/ga_normal_answer.json').read()))
gsc_api_mock    = MagicMock(return_value=json.loads(open('../test/api_responses/gsc_normal_answer.json').read()))
shitty_api_mock = MagicMock(return_value=json.dumps({"some":{"random":"bullshit","time":2}}))

@pytest.fixture(scope="module")
def config():
	return ConfigFactory("../test/config/config.yml")

'''UNIT TESTING'''
class TestConfigFactory(unittest.TestCase): 
	def test_instantiation(self): 
		config = ConfigFactory("config.yml")
		assert hasattr(config,"db_host")
		assert hasattr(config,"db_port")
		assert hasattr(config,"db_user")
		assert hasattr(config,"db_password")
		assert hasattr(config,"db_name")

	def test_typing(self): 
		config = ConfigFactory("config.yml")
		assert type(config.ga_dimensions) == list
		assert type(config.ga_metrics) == list
		assert type(config.gsc_dimensions) == list
		assert type(config.gsc_metrics) == list
		assert type(config.ga_view_id) == str
		assert type(config.db_host) == str
		assert type(config.db_port) == str
		assert type(config.ga_key_conf) == dict

	@patch.dict(os.environ,{'DB_HOST':'some host'} , clear=True)
	def test_missing_env_variable_raises_exception(self): 
		with self.assertRaises(Exception) as context:
			config = ConfigFactory("config.yml")

	@patch.dict(os.environ, {'DB_HOST':'bla','DB_PORT':'bla','INFLUXDB_USER':'bla','INFLUXDB_USER_PASSWORD':'bla','INFLUXDB_DB':'bla'}, clear=True)
	def test_missing_google_analytics_id_does_not_raise_excep(self): 
		config = ConfigFactory("config.yml")


class Test_GoogleExtractor: 
	"""test EXTRACT"""

	def test_data_interval_interval_is_one_day(self, config): 
		extractor  = GoogleExtractor(config)
		assert hasattr(extractor, "end_date")
		assert hasattr(extractor, "start_date")
		assert (extractor.end_date - extractor.start_date) == timedelta(days=1)

	def test_extractor_has_config_and_credentials(self, config): 
		extractor  = GoogleExtractor(config)
		assert type(extractor.config) == ConfigFactory
		assert type(extractor.credentials) == ServiceAccountCredentials

class Test_Batch_Google_Analytics:

	@patch('collector.GoogleExtractor._get_google_analytics_daily', ga_api_mock)
	def test_batch_has_proper_date(self, config): 
		batch  = GoogleExtractor(config).extract("google_analytics")
		assert batch.date == date.today()-timedelta(days=1)

	@patch('collector.GoogleExtractor._get_google_analytics_daily', ga_api_mock)
	def test_batch_extract_headers(self, config): 
		batch  = GoogleExtractor(config).extract("google_analytics")
		assert hasattr(batch, "metrics_names")
		assert hasattr(batch, "dimensions_names")
		assert batch.metrics_names == ["ga:sessions", "ga:pageLoadTime"]
		assert batch.dimensions_names == ["ga:deviceCategory", "ga:country"]

	@patch('collector.GoogleExtractor._get_google_analytics_daily', ga_api_mock)
	def test_batch_get_rows_properly(self, config): 
		batch  = GoogleExtractor(config).extract("google_analytics")
		assert hasattr(batch, "rows")
		assert next(batch.rows) == {'dimensions': ['desktop', 'France'], 'values': [8, 0]}

	@patch('collector.GoogleExtractor._get_google_analytics_daily', shitty_api_mock)
	def test_wrong_data_format_is_logged(self, config, caplog): 
		batch  = GoogleExtractor(config).extract("google_analytics")
		assert "Wrong data format from google_analytics" in caplog.text

class Test_Batch_Google_Search:

	@patch('collector.GoogleExtractor._get_google_search_daily', gsc_api_mock)
	def test_batch_right_source(self, config):
		batch  = GoogleExtractor(config).extract("google_search")
		assert batch.origin == "google_search"

	@patch('collector.GoogleExtractor._get_google_search_daily', gsc_api_mock)
	def test_batch_get_metrics_names_and_dimensions(self, config):
		batch  = GoogleExtractor(config).extract("google_search")
		assert batch.metrics_names == sorted(['clicks', 'impressions', 'ctr', 'position'])

	@patch('collector.GoogleExtractor._get_google_search_daily', gsc_api_mock)
	def test_batch_get_rows(self, config):
		batch  = GoogleExtractor(config).extract("google_search")
		assert hasattr(batch, "rows")
		assert next(batch.rows) == {'dimensions': ['data-major','https://www.data-major.com/'], 'values': [2.0,1.0,2.0,6.0]}

	@patch('collector.GoogleExtractor._get_google_search_daily', shitty_api_mock)
	def test_wrong_data_format_is_logged(self, config, caplog): 
		batch  = GoogleExtractor(config).extract("google_search")
		assert "Wrong data format from google_search" in caplog.text

	@patch('collector.GoogleExtractor._get_google_search_daily', MagicMock(return_value=json.loads(open('../test/api_responses/gsc_different_metric_order.json').read())))
	def test_different_metric_order_should_not_alter_rows(self, config): 
		batch  = GoogleExtractor(config).extract("google_search")
		assert hasattr(batch, "rows")
		assert next(batch.rows) == {'dimensions': ['data-major','https://www.data-major.com/'], 'values': [2.0,1.0,2.0,6.0]}

class TestDataFormater: 
	
	@patch('collector.GoogleExtractor._get_google_analytics_daily', ga_api_mock)
	def test_formater_for_google_analytics_and_influx(self, config): 
		batch  = GoogleExtractor(config).extract("google_analytics")
		db_client = InfluxDBClient(config.db_host, config.db_port, config.db_password, config.db_user, config.db_name)
		DataFormater(batch)>>db_client.write_points

		data = db_client.query('SELECT * FROM google_analytics')
		assert len(list(data.get_points(measurement='google_analytics'))) == 5
		assert list(data.get_points(measurement='google_analytics'))[0]["time"] == "{}T00:00:00Z".format(date.today()-timedelta(days=1))
		assert sum([x["ga:sessions"] for x in list(data.get_points(measurement='google_analytics', tags={'ga:deviceCategory': 'desktop'}))]) == 9

	@patch('collector.GoogleExtractor._get_google_search_daily', gsc_api_mock)
	def test_formater_for_google_search_and_influx(self, config): 
		batch  = GoogleExtractor(config).extract("google_search")
		db_client = InfluxDBClient(config.db_host, config.db_port, config.db_password, config.db_user, config.db_name)
		DataFormater(batch)>>db_client.write_points

		data = db_client.query('SELECT * FROM google_search')
		assert len(list(data.get_points(measurement='google_search'))) == 3
		assert sum([x["position"] for x in list(data.get_points(measurement='google_search', tags={'query': 'data-major'}))]) == 6.0

'''INTEGRATION TESTS'''

class TestMain: 

	@patch('collector.GoogleExtractor._get_google_search_daily', gsc_api_mock)
	@patch('collector.GoogleExtractor._get_google_analytics_daily', ga_api_mock)
	def test_ingestion_of_both(self, config, caplog): 
		caplog.set_level(logging.INFO)
		main()
		assert "Starting Google Analytics ingestion" in caplog.text
		assert "Starting Google Search ingestion" in caplog.text
	
	@patch('collector.GoogleExtractor._get_google_analytics_daily', ga_api_mock)
	def test_ingestion_that_missing_env_variable_prevent_ingestion(self, config, caplog): 
		caplog.set_level(logging.INFO)
		del os.environ['GS_URL']
		main()
		assert "Starting Google Analytics ingestion" in caplog.text
		assert "Starting Google Search ingestion" not in caplog.text

	@patch('collector.GoogleExtractor._get_google_search_daily', gsc_api_mock)
	def test_ingestion_that_missing_env_variable_prevent_ingestion(self, config, caplog): 
		caplog.set_level(logging.INFO)
		del os.environ['GA_VIEW_ID']
		main()
		assert "Starting Google Analytics ingestion" not in caplog.text
		assert "Starting Google Search ingestion" in caplog.text